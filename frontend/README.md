# BridgeDB Frontend

This is the lektor frontend for bridgedb. Unlike most web applications, this frontend is statically generated at build/install time.

## Dependencies

You'll need python3, pip, gettext, and venv. You can install these on debian with `sudo apt install python3 python3-pip gettext python3-venv`.

## Building

Normally this frontend will be built by the setup.py script in the repository root. However, you can build just the frontend for testing or development.

```bash
python3 -m venv venv
. venv/bin/activate

./build.sh dev
```

The static frontend will be built in the `public/` directory. You can view it by starting a webserver `python3 -m http.server -d public` and visiting `localhost:8000` in a web browser.

## Maintaining

Aside from the frontend's known bugs (see the Known Bugs section), the biggest maintenance task for the frontend is a periodic lego update.

```
git -C lego fetch && \
git -C lego checkout origin/main && \
git add lego && \
git commit
```

## Known Bugs

The BridgeDB frontend does have some known bugs. Most notably, the translated pages don't integrate well with the backend. Multiple attempts have been made to make the frontend and backend play nicely together, but so far none of them have worked.

## License

The BridgeDB frontend is licensed under the BSD-3-Clause license, same as the BridgeDB backend. For more information, refer to the LICENSE file in the root of this repository.
